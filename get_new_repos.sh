#!/bin/bash
set -e
if ssh -T git@gitlab.com >/dev/null; then
    proto=ssh
    extra=git@
else
    proto=https
    extra=
fi

new_repo() {
    if [ ! -d $1 ]; then
        git clone --recursive $2 $proto://${extra}gitlab.com/robigalia/$1.git
        if [ $(grep -c "/$1" .gitignore) = "0" ]; then
            echo "/$1" >> .gitignore;
        fi
    else
        echo Not cloning already-existing repo $1
    fi
}

for file in $(cat .subrepos); do
    new_repo $file
done
