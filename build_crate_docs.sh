#!/usr/bin/env bash

cratename() {
    case $1 in
        bitflags-core) echo bitflags;;
        rust-sel4) echo sel4;;
        sel4-start) echo sel4_start;;
        sel4-sys) echo sel4_sys;;
        *) echo $1;;
    esac
}

mkdir -p upstream_docs
d=$(pwd)/upstream_docs
dd=$d/i686-sel4-unknown/doc
truncate -s 0 $dd/index.html
cp meta/docs/style.css $dd
echo '<!doctype html> <html><head><meta charset="utf-8"><body>' >> $dd/index.html
cat < meta/docs/template-before.html >> $dd/index.html
echo "<p>Crates documented here:</p><ul>" >> $dd/index.html
for repo in bitflags-core pci rust-sel4 sel4-start sel4-sys virtio; do
    pushd $repo
    echo "<li><a href=\"https://doc.robigalia.org/$(cratename $repo)\">$(cratename $repo)</a></li>" >> $dd/index.html
    env CARGO_TARGET_DIR=$d cargo doc --target i686-sel4-unknown
    popd
done
echo "</ul>" >> $dd/index.html
cat < meta/docs/template-after.html >> $dd/index.html
echo "</body></html>" >> $dd/index.html
